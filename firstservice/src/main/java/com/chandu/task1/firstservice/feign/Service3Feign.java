package com.chandu.task1.firstservice.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.chandu.task1.firstservice.model.Input;

@FeignClient(name="service3",url="${service3.endpoint}")
public interface Service3Feign {

	@PostMapping("/v1/getConcatanatedString")
	public ResponseEntity<String> getConcatanatedString(@RequestBody Input input);
	
}
