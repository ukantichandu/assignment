package com.chandu.task1.firstservice.Iservice;

import com.chandu.task1.firstservice.model.Input;

public interface IService {
	
	String getResult(Input input);

}
