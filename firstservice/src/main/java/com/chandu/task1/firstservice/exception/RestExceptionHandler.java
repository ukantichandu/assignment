package com.chandu.task1.firstservice.exception;

import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_PROBLEM_JSON_VALUE;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.chandu.task1.firstservice.model.APIError;
import com.chandu.task1.firstservice.model.APIErrors;
import com.chandu.task1.firstservice.model.FeingApiError;

import feign.FeignException;

@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

	private final static Logger LOGGER = LoggerFactory.getLogger(RestExceptionHandler.class);

	private ResponseEntity<Object> errorResponse(HttpStatus status, HttpHeaders headers, String message) {
		final APIError apiError = APIError.builder().status(status.value()).detail(message)
				.build();

		headers.set(CONTENT_TYPE, APPLICATION_PROBLEM_JSON_VALUE);

		return ResponseEntity.status(status).headers(headers).body(apiError);
	}

	private ResponseEntity<Object> errorResponse(HttpStatus status, HttpHeaders headers, Set<String> messages) {
		final APIErrors apiError = APIErrors.builder().status(status.value()).detail(messages).build();

		headers.set(CONTENT_TYPE, APPLICATION_PROBLEM_JSON_VALUE);

		return ResponseEntity.status(status).headers(headers).body(apiError);
	}

	@ExceptionHandler({ HttpStatusCodeException.class })
	final ResponseEntity<Object> handleHttpException(Exception ex) {

		final HttpStatus status;
		String message;
		if (ex instanceof HttpStatusCodeException) {
			status = ((HttpStatusCodeException) ex).getStatusCode();
			message = ((HttpStatusCodeException) ex).getStatusText();
			if ("".equals(message))
				message = "Not Found";
		} else {
			message = ex.getMessage();
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			if (LOGGER.isWarnEnabled()) {
				LOGGER.warn("Unknown exception type: {}", ex.getClass().getName());
			}
		}
		return errorResponse(status, new HttpHeaders(), message);
	}

	@Override
	@NonNull
	protected ResponseEntity<Object> handleMethodArgumentNotValid(@NonNull MethodArgumentNotValidException ex,
			@NonNull HttpHeaders headers, @NonNull HttpStatus status, @NonNull WebRequest request) {
		List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();

		if (fieldErrors.size() < 2) {
			String message = fieldErrors.stream()
					.map((fieldError) -> String.format("Error found for parameter '%s'; %s", fieldError.getField(),
							fieldError.getDefaultMessage()))
					.findFirst().orElse(ex.getMessage());

			return errorResponse(status, headers, message);
		} else {
			Set<String> errors = new TreeSet<String>();
			fieldErrors.stream().map((fieldError) -> String.format("Error found for parameter '%s'; %s",
					fieldError.getField(), fieldError.getDefaultMessage())).forEach(errors::add);

			return errorResponse(status, headers, errors);
		}
	}
	
	@ExceptionHandler(FeignException.class)
    public ResponseEntity<Object> handleFeignException(FeignException ex, HttpServletResponse response) {
		final FeingApiError apiError = FeingApiError.builder()
    			.status(ex.status())
                .detail("Integration API error")
                .apiError(
                	FeingApiError.ApiError.builder()
                		.api(ex.getMessage())
                		.status(ex.status())
                		.response(ex.contentUTF8()).build())
                .build();

		LOGGER.error("FeignException >> " + apiError);
       
		response.setStatus(ex.status());
        return ResponseEntity.status(ex.status()).body(apiError);
    }

	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException exception,
			WebRequest request) {
		Set<ConstraintViolation<?>> violations = exception.getConstraintViolations();
		if (violations != null && violations.size() > 1) {
			Set<String> errors = new HashSet<String>(violations.size());
			violations.stream().map(cv -> cv.getMessage()).forEach(errors::add);
			return errorResponse(HttpStatus.BAD_REQUEST, new HttpHeaders(), errors);
		} else {
			Optional<ConstraintViolation<?>> constraintViolation = exception.getConstraintViolations().stream()
					.findFirst();
			if (constraintViolation.isPresent()) {
				return errorResponse(HttpStatus.BAD_REQUEST, new HttpHeaders(), constraintViolation.get().getMessage());
			}
			if (LOGGER.isWarnEnabled()) {
				LOGGER.warn("Unknown condition caused contraint violation, type : {}.", exception.getClass().getName());
			}
		}
		return errorResponse(HttpStatus.BAD_REQUEST, new HttpHeaders(), exception.getMessage());
	}

	@Override
	@NonNull
	protected ResponseEntity<Object> handleExceptionInternal(@NonNull Exception ex, Object body, HttpHeaders headers,
			HttpStatus status, @NonNull WebRequest request) {
		return errorResponse(status, headers, ex.getMessage());
	}

	@Override
	@NonNull
	protected ResponseEntity<Object> handleNoHandlerFoundException(@NonNull NoHandlerFoundException ex,
			@NonNull HttpHeaders headers, @NonNull HttpStatus status, @NonNull WebRequest request) {
		return errorResponse(status, headers, ex.getMessage());
	}
}
