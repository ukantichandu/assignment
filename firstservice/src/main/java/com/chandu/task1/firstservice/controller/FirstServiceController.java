package com.chandu.task1.firstservice.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chandu.task1.firstservice.Iservice.IService;
import com.chandu.task1.firstservice.model.Input;

@RequestMapping("/first/v1")
@RestController
@Validated
public class FirstServiceController {
	
	@Autowired
	IService serviceImpl;
	
	@GetMapping("/getStatus")
	ResponseEntity<String> getStatus(){
		
		return ResponseEntity.ok("Up");
	}
	
	@PostMapping("/result")
	ResponseEntity<String> getResult(@RequestBody @Valid Input input){
		return ResponseEntity.ok(serviceImpl.getResult(input));
	}

 	@GetMapping("/health")
 	ResponseEntity<Object> getHealth() {
 
		return ResponseEntity.ok().build();
 	}

}
