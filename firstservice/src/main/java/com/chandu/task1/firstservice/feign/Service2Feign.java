package com.chandu.task1.firstservice.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name="service2Feign",url= "${service2.endpoint}")
public interface Service2Feign {
	
	@GetMapping("/v1/gethello")
	public ResponseEntity<String> getHello();

}
