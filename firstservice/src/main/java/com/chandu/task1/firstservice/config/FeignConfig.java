package com.chandu.task1.firstservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import feign.Logger;
import feign.okhttp.OkHttpClient;

@Configuration
public class FeignConfig {

	@Bean
	public OkHttpClient client() {
		return new OkHttpClient();
	}

	@Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

}
