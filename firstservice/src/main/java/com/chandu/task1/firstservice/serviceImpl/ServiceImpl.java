package com.chandu.task1.firstservice.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.chandu.task1.firstservice.Iservice.IService;
import com.chandu.task1.firstservice.aop.LogMethod;
import com.chandu.task1.firstservice.feign.Service2Feign;
import com.chandu.task1.firstservice.feign.Service3Feign;
import com.chandu.task1.firstservice.model.Input;

@Service
public class ServiceImpl implements IService {

	@Autowired
	Service2Feign service2Feign;

	@Autowired
	Service3Feign service3Feign;

	@Override
	@LogMethod
	public String getResult(Input input) {

		ResponseEntity<String> responseEntity = service2Feign.getHello();

		if (HttpStatus.OK == responseEntity.getStatusCode()) {

			String hello = responseEntity.getBody();

			ResponseEntity<String> res = service3Feign.getConcatanatedString(input);

			if (HttpStatus.OK == res.getStatusCode()) {

				return hello + " " + res.getBody();
			}

		}

		return null;
	}

}
