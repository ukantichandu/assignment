package com.chandu.task1.firstservice.model;

import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class FeingApiError {

	int status;
	String detail;
	ApiError apiError;

	@Data
	@Builder
	public static class ApiError {
		String api;
		int status;
		String response;
	}
}
