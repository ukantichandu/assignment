package com.chandu.task1.secondservice.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chandu.task1.secondservice.aop.LogMethod;

@RequestMapping("/second/v1")
@RestController
@Validated
public class SecondServiceController {
	
	@GetMapping("/gethello")
	@LogMethod
	ResponseEntity<String> getHello() {
		
		return ResponseEntity.ok("Hello");
		
	}
	
	@GetMapping("/health")
	@LogMethod
	ResponseEntity<String> getHealth() {
		
		return ResponseEntity.ok().build();
		
	}

}
