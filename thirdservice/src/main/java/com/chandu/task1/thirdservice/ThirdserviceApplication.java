package com.chandu.task1.thirdservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThirdserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThirdserviceApplication.class, args);
	}

}
