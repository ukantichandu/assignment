package com.chandu.task1.thirdservice.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogAspect {

	Logger log = LoggerFactory.getLogger(LogAspect.class);

	@Around("@annotation(com.chandu.task1.thirdservice.aop.LogMethod)")
	public Object printMethodArguments(ProceedingJoinPoint pj) throws Throwable {

		log.info("Method is invoked : {}" , pj.getSignature());

		return pj.proceed();
	}

}
