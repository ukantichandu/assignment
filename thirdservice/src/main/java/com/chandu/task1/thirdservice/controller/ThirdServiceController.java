package com.chandu.task1.thirdservice.controller;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chandu.task1.thirdservice.aop.LogMethod;
import com.chandu.task1.thirdservice.model.Input;

@RequestMapping("/third/v1")
@RestController
@Validated
public class ThirdServiceController {

	@PostMapping("/getConcatanatedString")
	@LogMethod
	ResponseEntity<String> getConcatenatedString(@RequestBody @Valid Input input) {

		return ResponseEntity.ok(input.getName() + " " + input.getSurName());

	}
	
	@GetMapping("/health")
	@LogMethod
	ResponseEntity<Object> getHealth() {

		return ResponseEntity.ok().build();

	}
}
