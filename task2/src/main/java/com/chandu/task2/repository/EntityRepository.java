package com.chandu.task2.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.chandu.task2.entity.MyEntity;

public interface EntityRepository extends JpaRepository<MyEntity, Long> {
	
	
	
}
