package com.chandu.task2.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@Table(name = "entities")
@JsonInclude(Include.NON_NULL)
@Valid
public class MyEntity {

	public MyEntity() {
		super();
	}

	@Id
	@Min(value=1,message="id value must be greater than or equal to 1")
	private long id;

	private long parentId;

	private String name;

	private String color;

	public MyEntity(long id, int parentId, String name, String color) {
		super();
		this.id = id;
		this.parentId = parentId;
		this.name = name;
		this.color = color;
	}

	public long getParentId() {
		return parentId;
	}

	public void setParentId(long parentId) {
		this.parentId = parentId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public String toString() {
		return "MyEntity [id=" + id + ", parentId=" + parentId + ", name=" + name + ", color=" + color + "]";
	}

}
