package com.chandu.task2.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;

public class InvalidParameterException extends HttpStatusCodeException {

	private static final long serialVersionUID = 1L;

	public InvalidParameterException(String message) {
		super(HttpStatus.BAD_REQUEST, message);
	}
}