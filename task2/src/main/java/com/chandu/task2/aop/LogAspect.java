package com.chandu.task2.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogAspect {

	Logger log = LoggerFactory.getLogger(LogAspect.class);

	@Around("@annotation(com.chandu.task2.aop.LogMethodParam)")
	public Object printMethodArguments(ProceedingJoinPoint pj) throws Throwable {

		Object[] args = pj.getArgs();

		log.info("Method name : {}  and args are :" , pj.getSignature());

		for (int i = 0; i < args.length; i++) {

			log.info("arg {} is : {}",(i+1), args[i].toString());

		}
		return pj.proceed();
	}

}
