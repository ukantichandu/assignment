package com.chandu.task2.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chandu.task2.Iservice.IService;
import com.chandu.task2.entity.MyEntity;
import com.chandu.task2.model.EntityResponse;

@RestController
@RequestMapping("/task2")
@Validated
public class MyController {

	@Autowired
	IService serviceImpl;

	@GetMapping("/v1/all")
	public ResponseEntity<List<EntityResponse>> getAllEntities() {

		return ResponseEntity.ok(serviceImpl.getAllEntities());
	}

	@GetMapping("/v1/byId/{id}")
	public ResponseEntity<MyEntity> getById(@PathVariable @Min(value=1,message="id value must be greater than or equal to 1") long id) {

		return ResponseEntity.ok(serviceImpl.getEntityById(id));
	}

	@GetMapping("/v1/getData")
	public ResponseEntity<Object> postData() {
		serviceImpl.postData();
		return ResponseEntity.ok().build();
	}

	@PostMapping("/v1/postData")
	public ResponseEntity<Object> postData(@RequestBody @Valid List<MyEntity> entities) {
		serviceImpl.insertData(entities);
		return ResponseEntity.ok().build();
	}
	
	@DeleteMapping("/v1/cleanUp")
	public ResponseEntity<Object> deleteAll() {
		serviceImpl.deleteData();
		return ResponseEntity.noContent().build();
	}

	@GetMapping("/v1/health")
	public ResponseEntity<Object> getHealth() {
		return ResponseEntity.ok().build();
	}


}
