package com.chandu.task2.model;

import java.util.Set;

public class APIErrors {

	private Set<String> details;
	private int status;

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private int status;
		private Set<String> details;

		public Builder status(final int status) {
			this.status = status;
			return this;
		}

		public Builder detail(final Set<String> details) {
			this.details = details;
			return this;
		}

		public APIErrors build() {
			final APIErrors error = new APIErrors();
			error.setStatus(this.status);
			error.setDetails(this.details);
			return error;
		}
	}

	public Set<String> getDetails() {
		return details;
	}

	public void setDetails(Set<String> details) {
		this.details = details;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
}
