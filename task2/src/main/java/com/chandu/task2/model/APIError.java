package com.chandu.task2.model;

public class APIError {

	private String detail;
	private int status;

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private int status;
		private String detail;

		public Builder status(final int status) {
			this.status = status;
			return this;
		}

		public Builder detail(final String detail) {
			this.detail = detail;
			return this;
		}

		public APIError build() {
			final APIError error = new APIError();
			error.setStatus(this.status);
			error.setDetail(this.detail);
			return error;
		}
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
}
