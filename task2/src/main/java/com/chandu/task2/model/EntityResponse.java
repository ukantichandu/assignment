package com.chandu.task2.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class EntityResponse {
	

	private String name;
	
	private String color;
	
	private List<EntityResponse> subClasses;

	
	public EntityResponse() {
		super();
	}

	public EntityResponse(String name, String color, List<EntityResponse> subClasses) {
		super();
		this.name = name;
		this.color = color;
		this.subClasses = subClasses;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public List<EntityResponse> getSubClasses() {
		return subClasses;
	}

	public void setSubClasses(List<EntityResponse> subClasses) {
		this.subClasses = subClasses;
	}

	@Override
	public String toString() {
		return "EntityResponse [name=" + name + ", color=" + color + ", subClasses=" + subClasses + "]";
	}

}
