package com.chandu.task2.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import com.chandu.task2.Iservice.IService;
import com.chandu.task2.aop.LogMethodParam;
import com.chandu.task2.entity.MyEntity;
import com.chandu.task2.model.EntityResponse;
import com.chandu.task2.repository.EntityRepository;

@Service
public class ServiceImpl implements IService {

	@Autowired
	EntityRepository entityRepo;

	@Override
	public List<EntityResponse> getAllEntities() {

		List<MyEntity> myEntities = entityRepo.findAll();

		List<MyEntity> parentEntites = myEntities.stream().filter(m -> 0 == m.getParentId())
				.collect(Collectors.toList());

		List<EntityResponse> entityResponses = new ArrayList<EntityResponse>();

		parentEntites.forEach(pe -> {
			entityResponses.add(getChildEntities(pe, myEntities));
		});
		
		if(entityResponses.isEmpty()) {
			throw new HttpClientErrorException(HttpStatus.NO_CONTENT,"No Entity found");
		}

		return entityResponses;
	}
	
	
	EntityResponse getChildEntities(MyEntity pe,List<MyEntity> myEntities) {
		
		EntityResponse eRes = new EntityResponse();

		eRes.setColor(pe.getColor());
		eRes.setName(pe.getName());

		List<MyEntity> childEntities = myEntities.stream().filter(m -> m.getParentId() == pe.getId())
				.collect(Collectors.toList());

		if (!childEntities.isEmpty()) {
			eRes.setSubClasses(new ArrayList<EntityResponse>());

			childEntities.forEach(ce -> {
				eRes.getSubClasses().add(getChildEntities(ce, myEntities));
			});
		}
		return eRes;
	}

	@Override
	@LogMethodParam
	public MyEntity getEntityById(long id) {
		
		Optional<MyEntity> optionalEntity = entityRepo.findById(id);
		if(optionalEntity.isPresent())
			return optionalEntity.get();

		throw new HttpClientErrorException(HttpStatus.NOT_FOUND, String.format("No entity found with given ID : %d", id));
	}

	@Override
	public void postData() {
		
		List<MyEntity> myEntities = new ArrayList<MyEntity>();
		
		myEntities.add(new MyEntity(1, 0, "Warrior", "red"));
		myEntities.add(new MyEntity(2, 0, "Wizard", "green"));
		myEntities.add(new MyEntity(3, 0, "Priest", "white"));
		myEntities.add(new MyEntity(4, 0, "Rogue", "yellow"));
		myEntities.add(new MyEntity(5, 1, "Fighter", "blue"));
		myEntities.add(new MyEntity(6, 1, "Paladin", "lightblue"));
		myEntities.add(new MyEntity(7, 1, "Ranger", "lightgreen"));
		myEntities.add(new MyEntity(8, 2, "Mage", "grey"));
		myEntities.add(new MyEntity(9, 2, "Specialist wizard", "lightgrey"));
		myEntities.add(new MyEntity(10, 3, "Cleric", "red"));
		myEntities.add(new MyEntity(11, 3, "Druid", "green"));
		myEntities.add(new MyEntity(12, 3, "Priest of specific mythos", "white"));
		myEntities.add(new MyEntity(13, 4, "Thief", "yellow"));
		myEntities.add(new MyEntity(14, 4, "Bard", "blue"));
		myEntities.add(new MyEntity(15, 13, "Assassin", "lightblue"));
		
		entityRepo.saveAll(myEntities);

	}


	@Override
	@LogMethodParam
	public void insertData(List<MyEntity> entities) {
		
		entityRepo.saveAll(entities);
		
	}


	@Override
	public void deleteData() {
		
		entityRepo.deleteAll();
	}

}
