package com.chandu.task2.Iservice;

import java.util.List;

import com.chandu.task2.entity.MyEntity;
import com.chandu.task2.model.EntityResponse;

public interface IService {
	
	List<EntityResponse> getAllEntities();
	
	MyEntity getEntityById(long id);
	
	void postData();

	void insertData(List<MyEntity> entities);

	void deleteData();

}
